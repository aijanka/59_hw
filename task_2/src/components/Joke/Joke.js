import React from 'react';
import './Joke.css';

const Joke = props => {
    return (
        <div className="Joke">
            <button onClick={props.getNewJoke}>New joke from Chuck</button>
            <p>{props.jokeText}</p>
        </div>
    )
};

export default  Joke;
