import React, {Component} from 'react';
import Wrapper from "./hoc/Wrapper";
import Joke from "./components/Joke/Joke";

class App extends Component {
    state = {
        jokeText: ''
    }

    _makeRequest(url) {
        return fetch(url).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong with network request');
        });
    }

    componentDidMount() {
        const url = 'https://api.chucknorris.io/jokes/random';
        this._makeRequest(url).then(post => {
            this.setState({jokeText: post.value});
        })
    }

    render() {
        return (
            <Wrapper>
                {<Joke
                    jokeText={this.state.jokeText}
                    getNewJoke={() => this.componentDidMount()}
                />}
            </Wrapper>
        );
    }
}

export default App;
