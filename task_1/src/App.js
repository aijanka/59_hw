import React, {Component} from 'react';
import Wrapper from './hoc/Wrapper';
import InputForm from "./components/InputForm/InputForm";
import MovieItem from "./components/MovieItem/MovieItem";

class App extends Component {
    state = {
        items: [
            {movieName: 'some name', id: 1},
            {movieName: 'some name', id: 2},
            {movieName: 'some name', id: 3}
        ],
        currentMovieName: ''
    };

    saveCurrentMovieName = event => {
        this.setState({currentMovieName: event.target.value});
    };

    addNewMovie = () => {
        const items = [...this.state.items];
        const movieName = this.state.currentMovieName;
        items.push({movieName, id: Date.now()});
        this.setState({items, currentMovieName: ''});
    };

    deleteMovieItem = (id) =>{
        const items = [...this.state.items];
        const index = items.findIndex(item => item.id === id);
        items.splice(index, 1);
        this.setState({items});
    };

    saveCurrentMovieItemName = (event, id) => {
        const items = [...this.state.items];
        const index = items.findIndex(item => item.id === id);
        console.log(items[index])
        items[index].movieName = event.target.value;
        this.setState({items});
    };



    render() {
        return (
            <Wrapper>
                <InputForm
                    currentMovieName={(event) => this.saveCurrentMovieName(event)}
                    value={this.state.currentMovieName}
                    add={this.addNewMovie}

                />
                <div className='moviesWrapper'>
                    <span>To watch list:</span>
                    <Wrapper>
                        {this.state.items.map(item =>
                            <MovieItem
                                key={item.id}
                                currentName={(event) => this.saveCurrentMovieItemName(event, item.id)}
                                name={item.movieName}
                                delete={() => this.deleteMovieItem(item.id)}
                            />
                                            )
                        }
                    </Wrapper>
                </div>
            </Wrapper>
        );
    }
}

export default App;
