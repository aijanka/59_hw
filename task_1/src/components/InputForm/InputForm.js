import React from 'react';
import Wrapper from "../../hoc/Wrapper";

const InputForm = props => {
    return (
        <Wrapper>
            <input
                type="text"
                placeholder='Movie name'
                onChange={props.currentMovieName}
                value={props.value}


            />
            <button onClick={props.add}>Add</button>
        </Wrapper>
    )
};

export default InputForm;