import React, {Component} from 'react';

class MovieItem extends Component{
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.name !== this.props.name;
    }

    render(){
        return (
            <div className="MovieItem">
                <input
                    type="text"
                    value={this.props.name}
                    onChange={this.props.currentName}
                    key={this.props.id}
                />
                <button onClick={this.props.delete}>x</button>
            </div>
        )
    }
}

export default MovieItem;